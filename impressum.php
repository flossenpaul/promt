<?php
require("./init.php");

// Set the desktop icon in the top icon menue
$mainclasses = array("desktop" => "active",
    "profil" => "",
    "admin" => ""
    );
$template->assign("mainclasses", $mainclasses);
// create objects
$project = new project();
$customer = new company();
$milestone = new milestone();
$mtask = new task();
$msg = new message();

//create arrays to hold data
$messages = array();
$milestones = array();
$tasks = array();
//create a counter for the foreach loop
$cou = 0;



// Get todays date and count tasks, projects and messages for display
$today = date("d");
// Assign everything to the template engine
$template->assign("title", $title);
$template->assign("today", $today);

$template->assign("myprojects", $myOpenProjects);
$template->assign("oldprojects", $myClosedProjects);
$template->assign("projectnum", $projectnum);
$template->assign("closedProjectnum", $oldProjectnum);
$template->assign("projectov", "yes");

$template->assign("mode", $mode);

$template->assign("tasks", $etasks);
$template->assign("tasknum", $tasknum);

$template->assign("messages", $messages);
$template->assign("msgnum", $msgnum);
$template->display("impressum.tpl");

?>
